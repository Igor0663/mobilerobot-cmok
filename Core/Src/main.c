/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "PWM.h"
#include "encoder.h"
#include "motor_driver.h"
#include "pid.h"
#include "bh1750_driver.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#define START_BYTE 243
#define END_BYTE 217
#define BH1750_LEFT_ADDRESS (0x5C << 1)
#define BH1750_RIGHT_ADDRESS (0x23 << 1)
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
unsigned char rx = 0;
volatile uint8_t uart_flag = 0;
volatile uint32_t RPML, RPMR;

volatile uint32_t set_point_l, set_point_r;
volatile uint8_t bytes_received = 0;
volatile uint8_t autonomous_mode_flag = 0;
volatile uint8_t data_frame[6];
volatile int16_t L_setting, R_setting;
volatile uint16_t bh_left_lumen, bh_right_lumen;
dpid_t pid_l, pid_r;
bh1750_t bh_left, bh_right;
PID_InitTypeDef pid_config ={
	.p = 8.0f,
	.i = 30.0f,
	.d = 0.01f,
	.dt_ms = 10,
	.MAX_OUT = 999,
	.MIN_OUT = 0,
	.MAX_P = 999,
	.MIN_P = -999,
	.MAX_I = 999,
	.MIN_I = -999,
	.MAX_D = 999,
	.MIN_D = -999
};
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	if(__HAL_UART_GET_FLAG(huart, UART_FLAG_ORE) )
		__HAL_UART_CLEAR_OREFLAG(huart);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
   if(htim == &htim3)
   {
	   bh_left_lumen = bh1750_read_lumen(&bh_left);
	   RPML = Encoder_get_speed(2);
	   RPMR = Encoder_get_speed(1);
	   PWM_ChangeDutyCycle(2, pid_calc_output(&pid_l, set_point_l, RPML));
	   PWM_ChangeDutyCycle(1, pid_calc_output(&pid_r, set_point_r, RPMR));
	   bh_right_lumen = bh1750_read_lumen(&bh_right);
   }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart == & huart2)
		uart_flag = 1;
}

uint8_t check_data_frame()
{
	if(data_frame[0] != START_BYTE || data_frame[5] != END_BYTE ) return 0;
	if(data_frame[1] > 200 || data_frame[2] > 200 ) return 0;
	if(data_frame[3] > 1 || data_frame[4] > 1) return 0;
	return 1;
}
void setup_motor_driver()
{
	MotorDriver_set_mode(1, FORWARD);
	MotorDriver_set_mode(2, FORWARD);
}

void setup_encoders()
{
	Encoder_Init();
}

void setup_PWM()
{
	PWM_Init();
	PWM_ChangeDutyCycle(1, 0);
	PWM_ChangeDutyCycle(2, 0);
}

void setup_main_interrupt()
{
	HAL_TIM_Base_Start_IT(&htim3);
}

void setup_PID()
{
	pid_Init(&pid_l, &pid_config);
	pid_Init(&pid_r, &pid_config);
}

void setup_light_sensors()
{
	bh1750_init(&bh_left, &hi2c2 , BH1750_LEFT_ADDRESS);
	bh1750_set_mode(&bh_left, BH1750_MODE_CONTINUOUS_L);
	bh1750_init(&bh_right, &hi2c2 , BH1750_RIGHT_ADDRESS);
	bh1750_set_mode(&bh_right, BH1750_MODE_CONTINUOUS_L);
}


void vectors_to_speed(uint8_t front_vector, uint8_t side_vector)
{
	L_setting = ((int16_t)front_vector - 100 ) * 2;
	R_setting = ((int16_t)front_vector - 100 ) * 2;
	L_setting += ((int16_t)side_vector - 100) * 3 / 4;
	R_setting -= ((int16_t)side_vector - 100) * 3 / 4;

	if(L_setting > 0)
		MotorDriver_set_mode(2, FORWARD);
	else if(L_setting < 0)
	{
		L_setting = -L_setting;
		MotorDriver_set_mode(2, BACKWARD);
	}
	if(R_setting > 0)
		MotorDriver_set_mode(1, FORWARD);
	else if(R_setting < 0)
	{
		R_setting = -R_setting;
		MotorDriver_set_mode(1, BACKWARD);
	}
	set_point_l = (uint32_t)L_setting;
	set_point_r = (uint32_t)R_setting;
}

void set_speed_from_data_frame()
{
	vectors_to_speed(data_frame[1], data_frame[2]);
}

void set_speed_from_light_sensitivity()
{
	volatile uint8_t front_vector = 100, side_vector = 0;
	if(bh_right_lumen >= bh_left_lumen)
	{
		side_vector = (bh_right_lumen - bh_left_lumen)/10 + 100;
		side_vector = MIN(200, side_vector);
	}
	else
	{
		side_vector = (bh_left_lumen - bh_right_lumen)/10;
		side_vector = MIN( 100, side_vector);
		side_vector = 100 - side_vector;
	}

	vectors_to_speed(front_vector, side_vector);
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_I2C2_Init();
  MX_TIM4_Init();
  MX_USART2_UART_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
  setup_motor_driver();
  setup_encoders();
  setup_PWM();
  setup_PID();
  setup_light_sensors();
  HAL_UART_Receive_IT(&huart2, &rx, 1);
  setup_main_interrupt();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
	  if(uart_flag == 1)
	  {
		  if(rx == START_BYTE)
		  {
			 bytes_received = 0;
			 data_frame[bytes_received++] = rx;
		  }
		  else if(rx == END_BYTE)
		  {
			  if(bytes_received == 5)
			  {
				  data_frame[bytes_received++] = rx;
				  if(check_data_frame())
				  {
					  autonomous_mode_flag = data_frame[3];
					  if(autonomous_mode_flag)
						  set_speed_from_light_sensitivity();
					  else
						  set_speed_from_data_frame();
				  }
			  }
			  bytes_received = 0;
		  }
		  else
		  {
			  if(bytes_received >= 5 || bytes_received == 0)
			  {
				  bytes_received = 0;
			  }
			  else
				  data_frame[bytes_received++] = rx;
		  }
		  uart_flag = 0;
		  HAL_UART_Receive_IT(&huart2, &rx, 1);
	  }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
