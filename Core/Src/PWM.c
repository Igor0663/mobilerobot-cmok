#include "PWM.h"
#include "tim.h"
void PWM_Init()
{
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);
}
// 1 - ch1, 2 - ch2
void PWM_ChangeDutyCycle(uint8_t pwm_number, uint16_t dutycycle)
{
	if( dutycycle > 999)
		dutycycle = 999;
	if(pwm_number == 1)
		__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, dutycycle);
	else if( pwm_number == 2)
		__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_2, dutycycle);
}
