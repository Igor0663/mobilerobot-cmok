#include"pid.h"

void pid_Init(dpid_t* pid, PID_InitTypeDef* config)
{
	pid->config = config;
	pid->last_e = 0;
	pid->sum = 0;
	return;
}

uint32_t pid_calc_output(dpid_t* pid, uint32_t set_point, uint32_t measured_value)
{
	int32_t e = set_point - measured_value;

	pid->p_out = pid->config->p * e;
	if( pid->p_out > pid->config->MAX_P)
		pid->p_out = pid->config->MAX_P;
	if( pid->p_out < pid->config->MIN_P)
		pid->p_out = pid->config->MIN_P;

	pid->sum += e * pid->config->i * pid->config->dt_ms / 1000;
	pid->i_out = pid->sum;
	if( pid->i_out > pid->config->MAX_I)
		pid->i_out = pid->config->MAX_I;
	if( pid->i_out < pid->config->MIN_I)
		pid->i_out = pid->config->MIN_I;
	pid->sum = pid->i_out;

	pid->d_out = pid->config->d * (e - pid->last_e) * 1000 / pid->config->dt_ms;
	if( pid->d_out > pid->config->MAX_D)
		pid->d_out = pid->config->MAX_D;
	if( pid->d_out < pid->config->MIN_D)
		pid->d_out = pid->config->MIN_D;

	pid->pid_out = pid->p_out + pid->i_out + pid->d_out;
	if( pid->pid_out > pid->config->MAX_OUT)
		pid->pid_out = pid->config->MAX_OUT;
	if( pid->pid_out < pid->config->MIN_OUT)
		pid->pid_out = pid->config->MIN_OUT;

	pid->last_e = e;
	return pid->pid_out;
}
