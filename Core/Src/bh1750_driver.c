/*
 * bh1750_driver.c
 *
 *  Created on: May 30, 2021
 *      Author: Igor
 */

#include "bh1750_driver.h"
#include <math.h>

void bh1750_init(bh1750_t* sensor, I2C_HandleTypeDef* i2c_periph, uint8_t address)
{
	sensor->i2c_periph = i2c_periph;
	sensor->address = address;
	return;
}

void bh1750_write_to_device(bh1750_t* sensor, uint8_t* tx_buffer, uint16_t tx_length)
{
	HAL_I2C_Master_Transmit(sensor->i2c_periph, sensor->address, tx_buffer, tx_length, 5);
	return;
}

void bh1750_read_from_device(bh1750_t* sensor, uint8_t* rx_buffer, uint16_t rx_length)
{
	HAL_I2C_Master_Receive(sensor->i2c_periph, sensor->address, rx_buffer, rx_length, 5);
	return;
}

uint16_t bh1750_read_lumen(bh1750_t* sensor)
{
	uint8_t data[2] = {0};
	bh1750_read_from_device(sensor, data, 2);
	return (uint16_t) round( (float)(((uint16_t)(data[0]) << 8) + (uint16_t)(data[1])) / 1.2);
}

void bh1750_set_mode(bh1750_t* sensor, uint8_t mode)
{
	bh1750_write_to_device(sensor, &mode, 1);
	return;
}
