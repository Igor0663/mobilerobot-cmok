#include "motor_driver.h"

void MotorDriver_set_mode(uint8_t engine_number, motor_mode mode)
{
	GPIO_TypeDef* IN1_PORT = NULL;
	GPIO_TypeDef* IN2_PORT = NULL;
	uint16_t IN1_PIN = 0;
	uint16_t IN2_PIN = 0;
	switch(engine_number)
	{
		case 1:
			IN1_PORT = AIN_1_GPIO_Port;
			IN1_PIN  = AIN_1_Pin;
			IN2_PORT = AIN_2_GPIO_Port;
			IN2_PIN  = AIN_2_Pin;
			break;
		case 2:
			IN1_PORT = BIN_1_GPIO_Port;
			IN1_PIN  = BIN_1_Pin;
			IN2_PORT = BIN_2_GPIO_Port;
			IN2_PIN  = BIN_2_Pin;
			break;
		default:
			return;
	}
	if(mode & 0x01)
		HAL_GPIO_WritePin(IN2_PORT, IN2_PIN, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(IN2_PORT, IN2_PIN, GPIO_PIN_RESET);

	if(mode & 0x02)
		HAL_GPIO_WritePin(IN1_PORT, IN1_PIN, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(IN1_PORT, IN1_PIN, GPIO_PIN_RESET);
}

