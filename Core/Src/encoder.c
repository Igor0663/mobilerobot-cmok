#include "encoder.h"
#include "tim.h"
void Encoder_Init()
{
	HAL_TIM_Encoder_Start(&htim1, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);
}
uint16_t Encoder_get_speed(uint8_t encoder_number)
{
	uint16_t speed = 0, ticks = 0;
	switch(encoder_number)
	{
		case 1:
			ticks = __HAL_TIM_GET_COUNTER(&htim1);
			if(__HAL_TIM_IS_TIM_COUNTING_DOWN(&htim1))
				ticks = 65535 - ticks + 1;
			__HAL_TIM_SET_COUNTER(&htim1, 0);
			break;
		case 2:
			ticks = __HAL_TIM_GET_COUNTER(&htim2);
			if(__HAL_TIM_IS_TIM_COUNTING_DOWN(&htim2))
				ticks = 65535 - ticks + 1;
			__HAL_TIM_SET_COUNTER(&htim2, 0);
			break;
		default:
			break;
	}
	speed = ticks * 10/3;
	return speed;
}
