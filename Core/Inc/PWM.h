#pragma once
#include "stm32f1xx_hal.h"

void PWM_Init();
// 1 - ch1, 2 - ch2
void PWM_ChangeDutyCycle(uint8_t pwm_number, uint16_t dutycycle);
