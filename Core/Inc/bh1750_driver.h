/*
 * bh1750_driver.h
 *
 *  Created on: May 30, 2021
 *      Author: Igor Zieliński
 */

#ifndef INC_BH1750_DRIVER_H_
#define INC_BH1750_DRIVER_H_

#include "i2c.h"
#include "stdint.h"

#define BH1750_MODE_CONTINUOUS_H  0x10
#define BH1750_MODE_CONTINUOUS_H2 0x11
#define BH1750_MODE_CONTINUOUS_L  0x13
#define BH1750_MODE_SINGLE_H 	  0x20
#define BH1750_MODE_SINGLE_H2 	  0x21
#define BH1750_MODE_SINGLE_L 	  0x23


typedef struct
{
	I2C_HandleTypeDef* i2c_periph;
	uint8_t address;
} bh1750_t;

void bh1750_init(bh1750_t* sensor, I2C_HandleTypeDef* i2c_periph, uint8_t address);
void bh1750_write_to_device(bh1750_t* sensor, uint8_t* tx_buffer, uint16_t tx_length);
void bh1750_read_from_device(bh1750_t* sensor, uint8_t* rx_buffer, uint16_t rx_length);
uint16_t bh1750_read_lumen(bh1750_t* sensor);
void bh1750_set_mode(bh1750_t* sensor, uint8_t mode);
#endif /* INC_BH1750_DRIVER_H_ */
