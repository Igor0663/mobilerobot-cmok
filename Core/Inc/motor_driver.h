#pragma once
#include "stm32f1xx_hal.h"
#include "gpio.h"

#define BIN_1_Pin GPIO_PIN_0
#define BIN_1_GPIO_Port GPIOB
#define BIN_2_Pin GPIO_PIN_1
#define BIN_2_GPIO_Port GPIOB
#define AIN_1_Pin GPIO_PIN_14
#define AIN_1_GPIO_Port GPIOB
#define AIN_2_Pin GPIO_PIN_15
#define AIN_2_GPIO_Port GPIOB

typedef enum
{
	HARDBRAKE = 0,
	BACKWARD  = 1,
	FORWARD   = 2,
	SOFTBRAKE = 3,
} motor_mode;

void MotorDriver_set_mode(uint8_t engine_number, motor_mode mode);
