#pragma once
#include "stm32f1xx_hal.h"
void Encoder_Init();
uint16_t Encoder_get_speed(uint8_t encoder_number);
