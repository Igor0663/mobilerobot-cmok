/*
 * pid.h
 *
 *  Created on: May 28, 2021
 *      Author: Igor
 */

#ifndef INC_PID_H_
#define INC_PID_H_
#include <stdint.h>

typedef  struct
{
	float p;
	float i;
	float d;

	int32_t MAX_P;
	int32_t MIN_P;
	int32_t MAX_I;
	int32_t MIN_I;
	int32_t MAX_D;
	int32_t MIN_D;
	int32_t MAX_OUT;
	int32_t MIN_OUT;

	uint32_t dt_ms;


} PID_InitTypeDef;

typedef struct
{
	PID_InitTypeDef* config;
	int32_t sum;
	int32_t last_e;
	int32_t p_out;
	int32_t i_out;
	int32_t d_out;
	int32_t pid_out;
}dpid_t;

void pid_Init(dpid_t* pid, PID_InitTypeDef* config);
uint32_t pid_calc_output(dpid_t* pid, uint32_t set_point, uint32_t measured_value);
#endif /* INC_PID_H_ */
